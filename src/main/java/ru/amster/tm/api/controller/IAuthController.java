package ru.amster.tm.api.controller;

public interface IAuthController {

    void login();

    void logout();

    void registration();

}