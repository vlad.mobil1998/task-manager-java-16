package ru.amster.tm.api.servise;

import ru.amster.tm.role.Role;

public interface IAuthService {

    String getUserId();

    void login(String login, String password);

    void logout();

    void registration(String login, String password, String email);

    void checkRoles(Role[] roles);

}