package ru.amster.tm.command.auth;

import ru.amster.tm.command.AbstractCommand;

public abstract class AbstractAuthCommand extends AbstractCommand {

    UserActivated userActivated;

    public void setLoginCheck(UserActivated userActivated) {
        this.userActivated = userActivated;
    }

}