package ru.amster.tm.command.auth;

public class UserActivated {

    private boolean checkForActivation = false;

    public boolean getCheckForActivation() {
        return checkForActivation;
    }

    public void setCheckForActivation(boolean checkForActivation) {
        this.checkForActivation = checkForActivation;
    }

}