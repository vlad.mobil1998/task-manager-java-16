package ru.amster.tm.command.project;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.ProjectUtil;

import java.util.List;

public class ShowProjectsCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Show project list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECT]");
        final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final List<Project> projects = serviceLocator.getProjectService().findAll(userId);
        for (Project project : projects) ProjectUtil.showProject(project);
        System.out.println("[OK]");
    }

}