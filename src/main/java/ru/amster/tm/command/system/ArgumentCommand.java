package ru.amster.tm.command.system;

import ru.amster.tm.api.servise.ITerminalService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.service.TerminalService;

import java.util.Collection;

public class ArgumentCommand extends AbstractCommand {

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Display arguments program";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        ITerminalService terminalService = new TerminalService();
        final Collection<AbstractCommand> arguments = terminalService.getArguments();
        for (AbstractCommand argument : arguments) {
            System.out.println(argument.arg());
        }
    }

}