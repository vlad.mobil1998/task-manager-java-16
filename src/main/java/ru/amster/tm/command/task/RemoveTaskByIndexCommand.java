package ru.amster.tm.command.task;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class RemoveTaskByIndexCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-re-i";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Remove task by index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX");
        final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Integer maxIndex = serviceLocator.getProjectService().numberOfAllProjects(userId);
        if (index >= maxIndex) throw new InvalidIndexException(index, maxIndex);
        final Task task = serviceLocator.getTaskService().removeOneByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            throw new EmptyTaskException();
        }
        System.out.println("[OK]");
    }

}
