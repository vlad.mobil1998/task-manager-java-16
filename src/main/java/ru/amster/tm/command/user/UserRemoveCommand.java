package ru.amster.tm.command.user;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.role.Role;
import ru.amster.tm.util.TerminalUtil;

public class UserRemoveCommand extends AbstractCommand {

    @Override
    public String name() {
        return "re-user";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Delete user by login";
    }

    @Override
    public void execute() {
        System.out.println("[DELETED USER]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}