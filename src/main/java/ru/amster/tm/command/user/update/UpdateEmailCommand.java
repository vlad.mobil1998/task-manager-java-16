package ru.amster.tm.command.user.update;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.exception.empty.EmptyEmailException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class UpdateEmailCommand extends AbstractCommand {

    @Override
    public String name() {
        return "upd-email";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - updating user email";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE EMAIL]");
        System.out.println("ENTER EMAIL");
        final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final String email = TerminalUtil.nextLine();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        serviceLocator.getUserService().updateEmail(userId, email);
        System.out.println("[OK]");
    }

}