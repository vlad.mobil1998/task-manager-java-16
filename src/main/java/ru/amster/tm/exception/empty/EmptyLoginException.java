package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! LoginCommand is empty...");
    }

}