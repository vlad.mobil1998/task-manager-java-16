package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public class EmptyTaskException extends AbstractException {

    public EmptyTaskException() {
        super("Error! Task is empty (not found)...");
    }

}