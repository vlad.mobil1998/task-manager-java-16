package ru.amster.tm.exception.system;

import ru.amster.tm.exception.AbstractException;

public class InvalidCommandException extends AbstractException {

    public InvalidCommandException() {
        super("ERROR! Invalid command...");
    }

}