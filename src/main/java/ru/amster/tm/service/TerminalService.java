package ru.amster.tm.service;

import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.api.repository.ITaskRepository;
import ru.amster.tm.api.repository.IUserRepository;
import ru.amster.tm.api.servise.*;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.command.auth.*;
import ru.amster.tm.command.project.*;
import ru.amster.tm.command.system.*;
import ru.amster.tm.command.task.*;
import ru.amster.tm.command.user.*;
import ru.amster.tm.command.user.update.*;
import ru.amster.tm.repository.ProjectRepository;
import ru.amster.tm.repository.TaskRepository;
import ru.amster.tm.repository.UserRepository;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TerminalService implements ITerminalService {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private final UserActivated userActivated = new UserActivated();

    private static final Class[] COMMANDS = new Class[]{
            HelpCommand.class, AboutCommand.class, SystemInfoCommand.class,
            VersionCommand.class, CommandCommand.class, ArgumentCommand.class,

            UpdateEmailCommand.class, UpdateFirstNameCommand.class, UpdateLastNameCommand.class,
            UpdateMiddleNameCommand.class, ShowAllUsersCommand.class, UpdatePasswordCommand.class,
            UserUnlockCommand.class, UserLockCommand.class, UserRemoveCommand.class, UserProfileShowCommand.class,

            CreateProjectCommand.class, ShowProjectByIndexCommand.class, ShowProjectByNameCommand.class,
            ShowProjectByIdCommand.class, ShowProjectsCommand.class, UpdateProjectByIdCommand.class,
            UpdateProjectByIndexCommand.class, RemoveProjectByIdCommand.class,
            RemoveProjectByNameCommand.class, RemoveProjectByIndexCommand.class, ClearProjectCommand.class,

            CreateTaskCommand.class, ShowTaskByIndexCommand.class, ShowTaskByNameCommand.class,
            ShowTaskByIdCommand.class, ShowTasksCommand.class, UpdateTaskByIdCommand.class,
            UpdateTaskByIndexCommand.class, RemoveTaskByIdCommand.class, RemoveTaskByNameCommand.class,
            RemoveTaskByIndexCommand.class, ClearTaskCommand.class,

            ExitCommand.class,
    };
    private static final Class[] COMMANDS_AUTH = new Class[]{
            LoginCommand.class, LogoutCommand.class, RegistrationCommand.class,
    };
    private static final Class[] ARGUMENTS = new Class[]{
            HelpCommand.class, AboutCommand.class, SystemInfoCommand.class, VersionCommand.class,
    };

    {
        for (final Class clazz : COMMANDS) {
            try {
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand command = (AbstractCommand) commandInstance;
                command.setServiceLocator(this);
                commands.put(command.name(), command);
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
    }

    {
        for (final Class clazz : COMMANDS_AUTH) {
            try {
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand command = (AbstractCommand) commandInstance;
                command.setServiceLocator(this);
                final AbstractAuthCommand commandAuth = (AbstractAuthCommand) command;
                commandAuth.setLoginCheck(userActivated);
                commands.put(command.name(), command);
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
    }

    {
        for (final Class clazz : ARGUMENTS) {
            try {
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand argument = (AbstractCommand) commandInstance;
                arguments.put(argument.arg(), argument);
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public AbstractCommand getCommand(final String cmd) {
        return commands.get(cmd);
    }

    @Override
    public AbstractCommand getArgument(final String arg) {
        return arguments.get(arg);
    }

}