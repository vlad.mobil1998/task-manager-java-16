package ru.amster.tm.util;

import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyTaskException;

public interface TaskUtil {

    static void showTask(final Task task) {
        if (task == null) throw new EmptyTaskException();
        System.out.println("ID: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

}